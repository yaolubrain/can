train = {}
train.imageNum = 500000

function train.CAN(batchSize, epochNum, lr_add, lr_mul, epsilon)
  local CAU = net:get(3):get(1)
  local decodingUnits = net:get(4)
  local W, gradW = CAU:getParameters()
  local G, gradG = decodingUnits:getParameters()

  for i=1,epochNum do

    local x, y, z = data.getBatchRandom(train.imageNum, batchSize) 

  	net:zeroGradParameters()
  	local t = net:forward{x,y}
  	local err = criterion:forward(t, z)
  	local df = criterion:backward(t, z)
  	net:backward({x, y}, df)

    local feval = function()
      return err, gradG
    end

    optim.adam(feval, G, {learningRate=lr_add})

    local gradPos = 0.5 * (torch.abs(gradW) + gradW) + epsilon 
    local gradNeg = 0.5 * (torch.abs(gradW) - gradW) + epsilon
    W:cmul(torch.pow(torch.cdiv(gradNeg, gradPos), lr_mul)) 

    if i % 500 == 0 then
      lr_add = lr_add * 0.95
      lr_mul = lr_mul * 0.95
      print(i,err)
    end
  end
end

function train.NN(batchSize, epochNum, lr)
  local W, gradW = net:getParameters()

  for i=1,epochNum do

    local x, y, z = data.getBatchRandom(train.imageNum, batchSize) 

    net:zeroGradParameters()
    local t = net:forward{x,y}
    local err = criterion:forward(t, z)
    local df = criterion:backward(t, z)
    net:backward({x, y}, df)

    feval = function()
      return err, gradW
    end

    optim.adam(feval, W, {learningRate=lr})

    if i % 500 == 0 then
      lr = lr * 0.95
      print(i,err)
    end
  end
end

return train
