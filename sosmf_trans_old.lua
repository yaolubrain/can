require 'nn'
require 'cutorch'
require 'cunn'
require 'nngraph'
require 'optim'
require 'hdf5'
require 'data'
require 'SparseMax'

local imNum = 500000
local batchSize = 100
local inputDim = 121
local hSize1 = inputDim
local hSize2 = 1000
local hSize3 = 200
local max_z = 5
local epsilon = 1e-20


local file = hdf5.open('/share/bayes/yao/cifar10/cifar10_trans_train_11.h5', 'r')
local X = file:read('X'):all():float()
local Y = file:read('Y'):all():float()
local Z = file:read('Z'):all():float()
file:close()

X = X / 255 - 0.5
Y = Y / 255 - 0.5

function getData(n)
  local index = torch.random(1, imNum - n + 1)
  local x = X[{{index,index+n-1},{}}]:cuda()
  local y = Y[{{index,index+n-1},{}}]:cuda()
  local z = Z[{{index,index+n-1},{}}]:cuda()
  return x,y,z
end

input1 = nn.Identity()()
input2 = nn.Identity()()

l_RU = nn.SOSMF(hSize1,hSize1,hSize2,false,true)
l_RU.weight:abs()

l_integ = nn.Linear(hSize2, hSize3):noBias()
l_integ.weight:zero()
for i=1,hSize3 do
  local gSize = math.floor(hSize2/hSize3)
  l_integ.weight[{i,{(i-1)*gSize+1, i*gSize}}]:fill(1)
end

l_motion = nn.Sequential()
l_motion:add(l_RU)
l_motion:add(l_integ)

l_motion:add(nn.SoftMin())

l_output = nn.Sequential()
l_output:add(nn.Linear(hSize3,hSize3))
l_output:add(nn.PReLU())
l_output:add(nn.Linear(hSize3,2,false))

motion = l_motion({input1, input2})
output = l_output(motion)

net = nn.gModule({input1, input2}, {output})

criterion = nn.MSECriterion()

net:cuda()
criterion:cuda()

parameters, gradParameters = net:getParameters()

W = l_RU.weight
gradW = l_RU.gradWeight

W_integ = l_integ.weight
gradW_integ = l_integ.gradWeight

G, gradG = l_output:getParameters()

lr_mul = 0.005
lr_add = 0.001

for i=1,10000 do

  x,y,z = getData(batchSize)

	net:zeroGradParameters()
	t = net:forward{x,y}
	err = criterion:forward(t, z)
	df = criterion:backward(t, z)
	net:backward({x, y}, df)

	feval = function()
		return err, gradG
	end

  optim.adam(feval, G, {learningRate=lr_add})

  gradPosW = 0.5 * (torch.abs(gradW) + gradW) + epsilon 
  gradNegW = 0.5 * (torch.abs(gradW) - gradW) + epsilon
  W:cmul(torch.pow(torch.cdiv(gradNegW, gradPosW), lr_mul)) 

  if i % 500 == 0 then
    lr_add = lr_add * 0.9
    lr_mul = lr_mul * 0.8
  end

  print(i,err)

end

net:clearState()
torch.save('sosmf_trans.t7', net)
