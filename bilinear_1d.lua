require 'nn'
require 'cutorch'
require 'cunn'
require 'nngraph'
require 'optim'
require 'data'
require 'SparseMax'

local n = 20
local d = 11 
local hSize = 11 
local hSize2 = 11
local max_z = 5
local epsilon = 1e-8

l_encoder1 = nn.Sequential()
l_encoder2 = nn.Sequential()
l_encoder1:add(nn.Identity())
l_encoder2:add(nn.Identity())
--l_encoder1:add(l_f1)
--l_encoder2:add(l_f2)

l_encoder = nn.ParallelTable()
l_encoder:add(l_encoder1)
l_encoder:add(l_encoder2)

l_Bilinear = nn.Bilinear(hSize,hSize,hSize2,false,true)
--l_Bilinear.weight:abs()
l_motion = nn.Sequential()
l_motion:add(l_Bilinear)
l_motion:add(nn.Normalize(2))
--l_motion:add(nn.SoftMax())

l_output = nn.Sequential()
--l_output:add(nn.MulConstant(-1))
--l_output:add(nn.SparseMax())
--l_output:add(nn.Normalize(1))
--l_output:add(nn.SoftMax())
--l_output:add(nn.Normalize(2))
l_output:add(nn.Linear(hSize2,hSize2))
l_output:add(nn.PReLU())
l_output:add(nn.Linear(hSize2,1))

input1 = nn.Identity()()
input2 = nn.Identity()()

input_en = l_encoder({input1, input2})
motion = l_motion(input_en)
output = l_output(motion)

net = nn.gModule({input1, input2}, {output})

criterion = nn.MSECriterion()

parameters, gradParameters = net:getParameters()

W = l_Bilinear.weight
gradW = l_Bilinear.gradWeight

W_1, gradW_1 = l_encoder:getParameters()
W_2, gradW_2 = l_output:getParameters()

lr_mul = 0.01
lr_add = 0.0001

for i=1,100000 do

  x,y,z = data.random1D(n, d, max_z)

	net:zeroGradParameters()
	t = net:forward{x,y}
	err = criterion:forward(t, z)
	df = criterion:backward(t, z)
	net:backward({x, y}, df)

	feval1 = function()
		return err, gradW_1
	end
	feval2 = function()
		return err, gradW_2
	end
	feval3 = function()
		return err, gradParameters
	end


--  optim.adam(feval1, W_1, {learningRate=lr_add})
  optim.adam(feval3, parameters, {learningRate=lr_add})
--  optim.adam(feval2, W_2, {learningRate=lr_add})
--  optim.adadelta(feval1, W_1, {learningRate=lr_add})
--  optim.adadelta(feval2, W_2, {learningRate=lr_add})
--[[
  gradPos_f1 = 0.5 * (torch.abs(gradW_f1) + gradW_f1) + epsilon
  gradNeg_f1 = 0.5 * (torch.abs(gradW_f1) - gradW_f1) + epsilon
  W_f1:cmul(torch.pow(torch.cdiv(gradNeg_f1, gradPos_f1), lr_mul)) 
  gradPos = 0.5 * (torch.abs(gradW) + gradW) + epsilon
  gradNeg = 0.5 * (torch.abs(gradW) - gradW) + epsilon

  W:cmul(torch.pow(torch.cdiv(gradNeg, gradPos), lr_mul)) 

--]]
  if i % 500 == 0 then
    lr_add = lr_add * 0.9
    lr_mul = lr_mul * 0.9
  end

  print(i,err)

end

--print(nn.Threshold(1e-1):forward(W))
print(W)

x,y,z = data.random1D(10000, d, max_z)
t = net:forward{x,y}
err = criterion:forward(t, z)

print(err)

torch.save('sos_1d.t7', net)
