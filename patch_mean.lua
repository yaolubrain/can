require 'hdf5'

file = hdf5.open('/share/bayes/yao/ILSVRC2012_img_train/trainData/train_patches.h5', 'r')

n = 1281167

mean = torch.zeros(1, 121):float()

for i=1,n do
  local x = file:read('patch1'):partial({i,i}, {1,11}, {1,11}):resize(1,121):float()
  local y = file:read('patch2'):partial({i,i}, {1,11}, {1,11}):resize(1,121):float()

  mean:add(x:div(2*n))
  mean:add(y:div(2*n))

  if i % 10000 == 0 then
    print(i)
  end

end

print(mean)
torch.save('patch_mean.t7', mean)



