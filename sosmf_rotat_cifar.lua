require 'nn'
require 'cutorch'
require 'cunn'
require 'nngraph'
require 'optim'
require 'hdf5'
require 'data'
require 'SparseMax'

local n = 128
local d = 400
local hSize = d
local hSize2 = 1200
local hSize3 = 400
local epsilon = 1e-10

local file = hdf5.open('/share/bayes/yao/cifar10/cifar10_rotat_45_train.h5', 'r')
local X = file:read('X'):all():cuda()
local Y = file:read('Y'):all():cuda()
local Z = file:read('Z'):all():cuda()
file:close()

X = X / 255 - 0.5
Y = Y / 255 - 0.5

function data_rotat2D(n)
  local index = torch.random(1, 500000 - n + 1)
  local x = X[{{index,index+n-1},{}}]
  local y = Y[{{index,index+n-1},{}}]
  local z = Z[{{index,index+n-1},{}}]
  return x,y,z
end

input1 = nn.Identity()()
input2 = nn.Identity()()

l_RU = nn.SOSMF(hSize,hSize,hSize2,false,true)
l_RU.weight:abs()

l_pooling = nn.Linear(hSize2, hSize3):noBias()
l_pooling.weight:abs()
l_pooling.weight:zero()
for i=1,hSize3 do
  local gSize = math.floor(hSize2/hSize3)
  l_pooling.weight[{i,{(i-1)*gSize+1, i*gSize}}]:fill(1)
end


l_motion = nn.Sequential()
l_motion:add(l_RU)
l_motion:add(l_pooling)
l_motion:add(nn.SoftMin())


l_output = nn.Sequential()
l_output:add(nn.Linear(hSize3,hSize3/2))
l_output:add(nn.PReLU())
l_output:add(nn.Linear(hSize3/2,hSize3/4))
l_output:add(nn.PReLU())
l_output:add(nn.Linear(hSize3/4,1,false))

motion = l_motion({input1, input2})
output = l_output(motion)

net = nn.gModule({input1, input2}, {output})

criterion = nn.MSECriterion()

net:cuda()
criterion:cuda()

parameters, gradParameters = net:getParameters()

W = l_RU.weight
gradW = l_RU.gradWeight

P = l_pooling.weight
gradP = l_pooling.gradWeight

W_2, gradW_2 = l_output:getParameters()

lr_mul = 0.005
lr_add = 0.005

for i=1,200000 do

  x,y,z = data_rotat2D(n)

	net:zeroGradParameters()
	t = net:forward{x,y}
	err = criterion:forward(t, z)
	df = criterion:backward(t, z)
	net:backward({x, y}, df)

	feval1 = function()
    return err, gradParameters
	end

	feval2 = function()
		return err, gradW_2
	end
	feval3 = function()
		return err, gradW
	end

  optim.adam(feval2, W_2, {learningRate=lr_add})

  gradPos = 0.5 * (torch.abs(gradW) + gradW) + epsilon 
  gradNeg = 0.5 * (torch.abs(gradW) - gradW) + epsilon
  W:cmul(torch.pow(1 * torch.cdiv(gradNeg, gradPos), lr_mul)) 

--[[
  gradPosP = 0.5 * (torch.abs(gradP) + gradP) + epsilon 
  gradNegP = 0.5 * (torch.abs(gradP) - gradP) + epsilon
  P:cmul(torch.pow(1 * torch.cdiv(gradNegP, gradPosP), lr_mul)) 
--]]
  if i % 500 == 0 then
    lr_add = lr_add * 0.95
    lr_mul = lr_mul * 0.95
  end

  print(i,err)

end

n = 10000

x,y,z = data_rotat2D(n)
t = net:forward{x,y}
err = criterion:forward(t, z)

print(err)
net:clearState()
torch.save('sosmf_rotat_cifar.t7', net)
