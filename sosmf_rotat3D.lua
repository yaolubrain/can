require 'nn'
require 'cutorch'
require 'cunn'
require 'nngraph'
require 'optim'
require 'hdf5'
require 'data'
require 'SparseMax'

local n = 100
local d = 1024
local hSize = d
local hSize2 = 1200
local hSize3 = 400
local max_z = 5
local epsilon = 1e-10

local file = hdf5.open('/share/bayes/yao/coil-100/coil100.h5', 'r')
local img = file:read('img'):all():float()

img = img / 255 - 0.5

function data_rotat3D(n, c1, c2)

  X = torch.Tensor(n, d)
  Y = torch.Tensor(n, d)
  Z = torch.Tensor(n)

  for i=1,n do

    local z = torch.random(-max_z, max_z)

    local obj = torch.random(c1, c2)
    local orient1 = torch.random(1, 72)
    local orient2 = orient1 + z

    if orient2 > 72 then orient2 = orient2 - 72 end
    if orient2 < 1  then orient2 = orient2 + 72 end

    X[i] = img[{obj,orient1,{}}]
    Y[i] = img[{obj,orient2,{}}]
    Z[i] = z + max_z + 1
  end

  X = X:cuda()
  Y = Y:cuda()
  Z = Z:cuda()

  return X,Y,Z
end


l_RU = nn.SOSMF(hSize,hSize,hSize2,false,true)
l_RU.weight:abs()

l_pooling = nn.Linear(hSize2, hSize3):noBias()
l_pooling.weight:abs()

l_motion = nn.Sequential()
l_motion:add(l_RU)
l_motion:add(l_pooling)
l_motion:add(nn.SoftMin())

l_output = nn.Sequential()
l_output:add(nn.Linear(hSize3,hSize3/2))
l_output:add(nn.PReLU())
l_output:add(nn.Linear(hSize3/2, 2*max_z + 1))
l_output:add(nn.SoftMax())

input1 = nn.Identity()()
input2 = nn.Identity()()

motion = l_motion({input1, input2})
output = l_output(motion)

net = nn.gModule({input1, input2}, {output})

--criterion = nn.MSECriterion()
criterion = nn.CrossEntropyCriterion()

net:cuda()
criterion:cuda()

confusion = optim.ConfusionMatrix(2*max_z + 1)

parameters, gradParameters = net:getParameters()

W = l_RU.weight
gradW = l_RU.gradWeight

P = l_pooling.weight
gradP = l_pooling.gradWeight

W_2, gradW_2 = l_output:getParameters()

lr_mul = 0.005
lr_add = 0.005

for i=1,200000 do

  x,y,z = data_rotat3D(n, 1, 80)

	net:zeroGradParameters()
	t = net:forward{x,y}
	err = criterion:forward(t, z)
	df = criterion:backward(t, z)
	net:backward({x, y}, df)

	feval1 = function()
    return err, gradParameters
	end

	feval2 = function()
		return err, gradW_2
	end
	feval3 = function()
		return err, gradW
	end

  optim.adam(feval2, W_2, {learningRate=lr_add})

  gradPos = 0.5 * (torch.abs(gradW) + gradW) + epsilon 
  gradNeg = 0.5 * (torch.abs(gradW) - gradW) + epsilon
  W:cmul(torch.pow(1 * torch.cdiv(gradNeg, gradPos), lr_mul)) 

  gradPosP = 0.5 * (torch.abs(gradP) + gradP) + epsilon 
  gradNegP = 0.5 * (torch.abs(gradP) - gradP) + epsilon
  P:cmul(torch.pow(1 * torch.cdiv(gradNegP, gradPosP), lr_mul)) 

  for j=1,n do
    confusion:add(t[j], z[j])
  end

  if i % 500 == 0 then
    lr_add = lr_add * 0.95
    lr_mul = lr_mul * 0.95

    print(i,err)
    print(confusion)
    confusion:zero()
  end
end

n = 10000

x,y,z = data_rotat3D(n, 81, 100)
t = net:forward{x,y}
err = criterion:forward(t, z)

print(err)
confusion:zero()
for j=1,n do
  confusion:add(t[j], z[j])
end
print(confusion)


net:clearState()
torch.save('sosmf_rotat_45.t7', net)
