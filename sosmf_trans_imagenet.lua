require 'nn'
require 'cutorch'
require 'cunn'
require 'nngraph'
require 'optim'
require 'hdf5'
require 'data'
require 'SparseMax'

local batchSize = 100
local inputDim = 400
local hSize1 = inputDim
local hSize2 = 1200
local hSize3 = 400
local epsilon = 1e-20
local imNum = 1281167

local file = hdf5.open('/share/bayes/yao/imagenet/data/trans_train.h5', 'r')
local X = file:read('X'):all():float()
local Y = file:read('Y'):all():float()
local Z = file:read('Z'):all():float()
file:close()

X = X / 255 - 0.5
Y = Y / 255 - 0.5

function getData(batchSize)
  local index = torch.random(1, imNum - batchSize + 1)
  local x = X[{{index,index+batchSize-1},{}}]:cuda()
  local y = Y[{{index,index+batchSize-1},{}}]:cuda()
  local z = Z[{{index,index+batchSize-1},{}}]:cuda()
  return x,y,z
end

input1 = nn.Identity()()
input2 = nn.Identity()()

l_RU = nn.SOSMF(hSize1,hSize1,hSize2,false,true)
l_RU.weight:abs()

l_pooling = nn.Linear(hSize2,hSize3):noBias()
l_pooling.weight:abs()
l_pooling.weight:zero()
for i=1,hSize3 do
  local gSize = math.floor(hSize2/hSize3)
  l_pooling.weight[{i,{(i-1)*gSize+1, i*gSize}}]:fill(1)
end

l_motion = nn.Sequential()
l_motion:add(l_RU)
l_motion:add(l_pooling)
l_motion:add(nn.SoftMin())

l_output = nn.Sequential()
l_output:add(nn.Linear(hSize3,hSize3))
l_output:add(nn.PReLU())
l_output:add(nn.Linear(hSize3,hSize3))
l_output:add(nn.PReLU())
l_output:add(nn.Linear(hSize3,2,false))

motion = l_motion({input1, input2})
output = l_output(motion)

net = nn.gModule({input1, input2}, {output})
net:cuda()

criterion = nn.MSECriterion()
criterion:cuda()

parameters, gradParameters = net:getParameters()

W = l_RU.weight
gradW = l_RU.gradWeight

P = l_pooling.weight
gradP = l_pooling.gradWeight

G, gradG = l_output:getParameters()

lr_mul = 0.005
lr_add = 0.001

for i=1,200000 do

  x,y,z = getData(batchSize)

	net:zeroGradParameters()
	t = net:forward{x,y}
	err = criterion:forward(t, z)
	df = criterion:backward(t, z)
	net:backward({x, y}, df)

	feval = function()
		return err, gradG
	end

  optim.adam(feval, G, {learningRate=lr_add})

  gradPosW = 0.5 * (torch.abs(gradW) + gradW) + epsilon 
  gradNegW = 0.5 * (torch.abs(gradW) - gradW) + epsilon
  W:cmul(torch.pow(torch.cdiv(gradNegW, gradPosW), lr_mul)) 

--[[
  gradPosP = 0.5 * (torch.abs(gradP) + gradP) + epsilon 
  gradNegP = 0.5 * (torch.abs(gradP) - gradP) + epsilon
  P:cmul(torch.pow(torch.cdiv(gradNegP, gradPosP), lr_mul)) 
--]]
  if i % 500 == 0 then
--[[ Works very well. Error around: 0.1
    lr_add = lr_add * 0.95
    lr_mul = lr_mul * 0.9
--]]
    lr_add = lr_add * 0.95
    lr_mul = lr_mul * 0.95
  end

  print(i,err)

end

net:clearState()
torch.save('sosmf_trans.t7', net)
