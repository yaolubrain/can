data = {}

function data.train(task)
  local file = nil
  if task == 'translation' then 
    file = hdf5.open('/share/bayes/yao/cifar10/translation_train.h5', 'r')
  elseif task == 'rotation' then
    file = hdf5.open('/share/bayes/yao/cifar10/rotation_train.h5', 'r')
  elseif task == 'scaling' then
    file = hdf5.open('/share/bayes/yao/cifar10/scaling_train.h5', 'r')
  elseif task == 'affine' then
    file = hdf5.open('/share/bayes/yao/cifar10/affine_train.h5', 'r')
  elseif task == 'projective' then
    file = hdf5.open('/share/bayes/yao/cifar10/projective_train.h5', 'r')
  end

  local X = file:read('X'):all():float()
  local Y = file:read('Y'):all():float()
  local Z = file:read('Z'):all():float()
  file:close()
  X = X / 255 - 0.5
  Y = Y / 255 - 0.5
  return X, Y, Z
end

function data.test(task)
  local file = nil
  if task == 'translation' then 
    file = hdf5.open('/share/bayes/yao/cifar10/translation_test.h5', 'r')
  elseif task == 'rotation' then
    file = hdf5.open('/share/bayes/yao/cifar10/rotation_test.h5', 'r')
  elseif task == 'scaling' then
    file = hdf5.open('/share/bayes/yao/cifar10/scaling_test.h5', 'r')
  elseif task == 'affine' then
    file = hdf5.open('/share/bayes/yao/cifar10/affine_test.h5', 'r')
  elseif task == 'projective' then
    file = hdf5.open('/share/bayes/yao/cifar10/projective_test.h5', 'r')
  end

  local X = file:read('X'):all():float()
  local Y = file:read('Y'):all():float()
  local Z = file:read('Z'):all():float()
  file:close()
  X = X / 255 - 0.5
  Y = Y / 255 - 0.5
  return X, Y, Z
end

function data.getBatch(i, batchSize)
  local x = X[{{(i-1)*batchSize+1,i*batchSize},{}}]:cuda()
  local y = Y[{{(i-1)*batchSize+1,i*batchSize},{}}]:cuda()
  local z = Z[{{(i-1)*batchSize+1,i*batchSize},{}}]:cuda()
  return x, y, z
end

function data.getBatchRandom(imageNum, batchSize)
  local index = torch.random(1, imageNum - batchSize + 1)
  local x = X[{{index,index+batchSize-1},{}}]:cuda()
  local y = Y[{{index,index+batchSize-1},{}}]:cuda()
  local z = Z[{{index,index+batchSize-1},{}}]:cuda()
  return x, y, z
end

return data
