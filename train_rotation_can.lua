require 'nn'
require 'cutorch'
require 'cunn'
require 'nngraph'
require 'optim'
require 'hdf5'
require 'data'
require 'model'
require 'train'

local inputDim = 121
local outputDim = 1
local lr_mul = 0.005
local lr_add = 0.005
local epsilon = 1e-20
local batchSize = 100
local epochNum = 200000

net = model.CAN({inputDim, 1200, 300, 100, 100, outputDim})
net:cuda()

criterion = nn.MSECriterion()
criterion:cuda()

X, Y, Z = data.train('rotation')
train.CAN(batchSize, epochNum, lr_add, lr_mul, epsilon)

net:clearState()
torch.save('models/can_rotation.t7', net)
