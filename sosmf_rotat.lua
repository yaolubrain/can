require 'nn'
require 'cutorch'
require 'cunn'
require 'nngraph'
require 'optim'
require 'hdf5'
require 'data'
require 'model'
require 'train'

local batchSize = 100
local inputDim = 121
local hSize1 = inputDim
local hSize2 = 1600
local hSize3 = 400
local outputSize = 1
local epsilon = 1e-20
local lr_mul = 0.005
local lr_add = 0.005
local epochNum = 200000

net = model.SOSNet(hSize1, hSize2, hSize3, outputSize)
net:cuda()

criterion = nn.MSECriterion()
criterion:cuda()

X, Y, Z = data.rotat()
train.SOSNet(batchSize, epochNum, lr_add, lr_mul, epsilon)

net:clearState()
torch.save('sosmf_rotat.t7', net)
