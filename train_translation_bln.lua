require 'nn'
require 'cutorch'
require 'cunn'
require 'nngraph'
require 'optim'
require 'hdf5'
require 'data'
require 'train'
require 'model'

local inputDim = 121
local outputDim = 2
local lr_add = 0.005
local batchSize = 100
local epochNum = 200000

net = model.BLN({inputDim, 1200, 300, 100, 100, outputDim})
net:cuda()

criterion = nn.MSECriterion()
criterion:cuda()

X, Y, Z = data.train('translation')
train.NN(batchSize, epochNum, lr_add)

net:clearState()
torch.save('models/bln_translation.t7', net)
