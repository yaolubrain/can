test = {}
test.imageNum = 100000

function test.computeParameterError(model)
  local batchSize = 1000
  local batchNum  = test.imageNum / batchSize
  local errors = torch.Tensor(test.imageNum, 1):cuda()
  for i=1,batchNum do
    local x, y, z = data.getBatch(i, batchSize)
    local t = model:forward{x,y}
    errors[{{(i-1)*batchSize+1,i*batchSize}}] = criterion:forward(t, z)
  end
  return errors
end

function test.computeTransformationError(model, task)
  local batchSize = 1000
  local batchNum  = test.imageNum / batchSize
  local errors = torch.zeros(test.imageNum, 1):cuda()
  local P = torch.Tensor({{0,0,1}, {1,0,1}, {1,1,1}, {0,1,1}})

  for i=1,batchNum do
    local x, y, z = data.getBatch(i, batchSize)
    local t = model:forward{x,y}
    for j=1,batchSize do
      local H_truth = torch.eye(3)
      local H_infer = torch.eye(3)
      if task == 'translation' then
        H_truth[{1,3}] = z[{j,1}]
        H_truth[{2,3}] = z[{j,2}]
        H_infer[{1,3}] = t[{j,1}]
        H_infer[{2,3}] = t[{j,2}]
      elseif task == 'rotation' then
        H_truth[{1,1}] =  math.cos(math.rad(z[{j,1}]))
        H_truth[{1,2}] = -math.sin(math.rad(z[{j,1}]))
        H_truth[{2,1}] =  math.sin(math.rad(z[{j,1}]))
        H_truth[{2,2}] =  math.cos(math.rad(z[{j,1}]))
        H_infer[{1,1}] =  math.cos(math.rad(t[{j,1}]))
        H_infer[{1,2}] = -math.sin(math.rad(t[{j,1}]))
        H_infer[{2,1}] =  math.sin(math.rad(t[{j,1}]))
        H_infer[{2,2}] =  math.cos(math.rad(t[{j,1}]))
      elseif task == 'scaling' then
        H_truth[{1,1}] = z[{j,1}]
        H_truth[{2,2}] = z[{j,2}]
        H_infer[{1,1}] = t[{j,1}]
        H_infer[{2,2}] = t[{j,2}]
      elseif task == 'affine' then
        H_truth[{1,1}] = H_truth[{1,1}] + z[{j,1}]
        H_truth[{1,2}] = H_truth[{1,2}] + z[{j,2}]
        H_truth[{2,1}] = H_truth[{2,1}] + z[{j,3}]
        H_truth[{2,2}] = H_truth[{2,2}] + z[{j,4}]
        H_infer[{1,1}] = H_infer[{1,1}] + t[{j,1}]
        H_infer[{1,2}] = H_infer[{1,2}] + t[{j,2}]
        H_infer[{2,1}] = H_infer[{2,1}] + t[{j,3}]
        H_infer[{2,2}] = H_infer[{2,2}] + t[{j,4}]
      elseif task == 'projective' then
        H_truth[{1,1}] = H_truth[{1,1}] + z[{j,1}]
        H_truth[{1,2}] = H_truth[{1,2}] + z[{j,2}]
        H_truth[{2,1}] = H_truth[{2,1}] + z[{j,3}]
        H_truth[{2,2}] = H_truth[{2,2}] + z[{j,4}]
        H_truth[{1,3}] = H_truth[{1,3}] + z[{j,5}]
        H_truth[{2,3}] = H_truth[{2,3}] + z[{j,6}]
        H_infer[{1,1}] = H_infer[{1,1}] + t[{j,1}]
        H_infer[{1,2}] = H_infer[{1,2}] + t[{j,2}]
        H_infer[{2,1}] = H_infer[{2,1}] + t[{j,3}]
        H_infer[{2,2}] = H_infer[{2,2}] + t[{j,4}]
        H_infer[{1,3}] = H_infer[{1,3}] + t[{j,5}]
        H_infer[{2,3}] = H_infer[{2,3}] + t[{j,6}]
      end

      local PT_truth = torch.Tensor(4, 3)
      local PT_infer = torch.Tensor(4, 3)
      local norm_sum = 0
      for k=1,4 do
        PT_truth[k] = H_truth * P[k]
        PT_infer[k] = H_infer * P[k]
        norm_sum = norm_sum + torch.norm(PT_truth[k])
      end
      local err = 0
      for k=1,4 do
        err = err + torch.norm(PT_truth[k] - PT_infer[k]) / norm_sum
      end
      errors[(i-1)*batchSize + j] = err
    end
  end
  return errors
end

return test
