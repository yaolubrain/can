require 'nn'
require 'cutorch'
require 'cunn'
require 'optim'
require 'hdf5'

local n = 100
local d = 1024
local hSize = 200
local hSize2 = 1200
local hSize3 = 100
local epsilon = 1e-10
local max_z = 5


local file = hdf5.open('/share/bayes/yao/coil-100/coil100.h5', 'r')
local img = file:read('img'):all():float()

img = img / 255 - 0.5

function data_rotat3D(n, c1, c2)

  X = torch.Tensor(n, d)
  Y = torch.Tensor(n, d)
  Z = torch.Tensor(n)

  for i=1,n do

    local z = torch.random(-max_z, max_z)

    local obj = torch.random(c1, c2)
    local orient1 = torch.random(1, 72)
    local orient2 = orient1 + z

    if orient2 > 72 then orient2 = orient2 - 72 end
    if orient2 < 1  then orient2 = orient2 + 72 end

    X[i] = img[{obj,orient1,{}}]
    Y[i] = img[{obj,orient2,{}}]
    Z[i] = z + max_z + 1
  end

  X = X:cuda()
  Y = Y:cuda()
  Z = Z:cuda()

  return X,Y,Z
end


local net = nn.Sequential()
net:add(nn.JoinTable(2, 4))
net:add(nn.Linear(2*d, hSize))
net:add(nn.PReLU())
net:add(nn.Linear(hSize, hSize))
net:add(nn.PReLU())
net:add(nn.Linear(hSize, 2*max_z + 1))
net:add(nn.SoftMax())

--criterion = nn.MSECriterion()
--criterion = nn.ClassNLLCriterion()
criterion = nn.CrossEntropyCriterion()
net:cuda()
criterion:cuda()

confusion = optim.ConfusionMatrix(2*max_z + 1)

W, gradW = net:getParameters()

lr = 0.001

for i=1,200000 do

  x,y,z = data_rotat3D(n, 1, 80)

	feval = function()
  	net:zeroGradParameters()
   	t = net:forward{x,y}
  	err = criterion:forward(t, z)
  	df = criterion:backward(t, z)
  	net:backward({x, y}, df)
		return err, gradW
	end

  optim.adam(feval, W, {learningRate=lr})
  for j=1,n do
    confusion:add(t[j], z[j])
  end


  if i % 500 == 0 then
    lr = lr * 0.9

    print(i,err)
    print(confusion)
    confusion:zero()
  end

--  print(i,err)

end

n = 5000
x,y,z = data_rotat3D(n, 81, 100)
t = net:forward{x,y}
err = criterion:forward(t, z)

print(err)
confusion:zero()
for j=1,n do
  confusion:add(t[j], z[j])
end
print(confusion)


net:clearState()
torch.save('mlp_rotat3D.t7', net)
