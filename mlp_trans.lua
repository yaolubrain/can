require 'nn'
require 'cutorch'
require 'cunn'
require 'nngraph'
require 'optim'
require 'hdf5'
require 'data'
require 'SparseMax'


local batchSize = 100
local inputDim = 121
local hSize1 = 800
local hSize2 = 400
local hSize3 = 200
local hSize4 = 200
local imNum = 500000

local file = hdf5.open('/share/bayes/yao/cifar10/cifar10_trans_train_11.h5', 'r')
local X = file:read('X'):all():float()
local Y = file:read('Y'):all():float()
local Z = file:read('Z'):all():float()
file:close()

X = X / 255 - 0.5
Y = Y / 255 - 0.5

function getData(batchSize)
  local index = torch.random(1, imNum - batchSize)
  local x = X[{{index,index+batchSize},{}}]:cuda()
  local y = Y[{{index,index+batchSize},{}}]:cuda()
  local z = Z[{{index,index+batchSize},{}}]:cuda()
  return x,y,z
end

local net = nn.Sequential()
net:add(nn.JoinTable(2, 4))
net:add(nn.Linear(2*inputDim, hSize1))
net:add(nn.PReLU())
net:add(nn.Linear(hSize1, hSize2))
net:add(nn.PReLU())
net:add(nn.Linear(hSize2, hSize3))
net:add(nn.PReLU())
net:add(nn.Linear(hSize3, 2))

net:cuda()

criterion = nn.MSECriterion()
criterion:cuda()

W, gradW = net:getParameters()

lr = 0.001

for i=1,200000 do

  x,y,z = getData(batchSize)
  x = x:cuda()
  y = y:cuda()
  z = z:cuda()

	net:zeroGradParameters()
	t = net:forward{x,y}
	err = criterion:forward(t, z)
	df = criterion:backward(t, z)
	net:backward({x, y}, df)

	feval = function()
		return err, gradW
	end

  optim.adam(feval, W, {learningRate=lr})

  if i % 500 == 0 then
    lr = lr * 0.95
  end

  print(i,err)

end

net:clearState()

torch.save('mlp_trans.t7', net)
