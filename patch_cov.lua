require 'hdf5'

file = hdf5.open('/share/bayes/yao/ILSVRC2012_img_train/trainData/train_patches.h5', 'r')

n = 1281167

mean = torch.load('patch_mean.t7')

cov  = torch.zeros(121, 121):float()

for i=1,n do
  local x = file:read('patch1'):partial({i,i}, {1,11}, {1,11}):resize(1,121):float()
  local y = file:read('patch2'):partial({i,i}, {1,11}, {1,11}):resize(1,121):float()

  x:csub(mean)
  y:csub(mean)

  local cov_x = x:t() * x
  local cov_y = y:t() * y
  cov_x:div(2*n)
  cov_y:div(2*n)

  cov:add(cov_x)
  cov:add(cov_y)


  print(x)
 
  if i % 10000 == 0 then
    print(i)
  end

end

torch.save('patch_cov.t7', cov)

print(cov)


