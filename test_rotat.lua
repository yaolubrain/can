require 'nn'
require 'cutorch'
require 'cunn'
require 'nngraph'
require 'optim'
require 'hdf5'
require 'data'

batchSize = 10000
batchNum  = 10

mlp = torch.load('mlp_rotat.t7'):cuda()
sos = torch.load('sosmf_rotat.t7'):cuda()

criterion = nn.MSECriterion()
criterion:cuda()

file = hdf5.open('/share/bayes/yao/cifar10/cifar10_rotat_45_test.h5', 'r')
--file = hdf5.open('/share/bayes/yao/imagenet/data/rotat_test.h5', 'r')
local X = file:read('X'):all():float()
local Y = file:read('Y'):all():float()
local Z = file:read('Z'):all():float()
file:close()

X = X / 255 - 0.5
Y = Y / 255 - 0.5

function getData(i)
  local x = X[{{(i-1)*batchSize+1,i*batchSize},{}}]:cuda()
  local y = Y[{{(i-1)*batchSize+1,i*batchSize},{}}]:cuda()
  local z = Z[{{(i-1)*batchSize+1,i*batchSize},{}}]:cuda()
  return x,y,z
end

err = 0
for i=1,batchNum do
  x,y,z = getData(i)
  t = mlp:forward{x,y}
  err =  err + criterion:forward(t, z)
end
print(err/batchNum)

err = 0
for i=1,batchNum do
  local x,y,z = getData(i)
  t = sos:forward{x,y}
  err = err + criterion:forward(t, z)
end

print(err/batchNum)

