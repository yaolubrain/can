require 'nn'
require 'cutorch'
require 'cunn'
require 'optim'
require 'hdf5'

local batchSize = 100
local inputDim = 121
local imNum = 500000

local hSize1 = 1200
local hSize2 = 800
local hSize3 = 400
local epsilon = 1e-10

--local file = hdf5.open('/share/bayes/yao/imagenet/data/rotat_train.h5', 'r')

local file = hdf5.open('/share/bayes/yao/cifar10/cifar10_rotat_45_train.h5', 'r')
local X = file:read('X'):all():float()
local Y = file:read('Y'):all():float()
local Z = file:read('Z'):all():float()
file:close()

X = X / 255 - 0.5
Y = Y / 255 - 0.5

function getData(batchSize)
  local index = torch.random(1, imNum - batchSize + 1)
  local x = X[{{index,index+batchSize-1},{}}]:cuda()
  local y = Y[{{index,index+batchSize-1},{}}]:cuda()
  local z = Z[{{index,index+batchSize-1},{}}]:cuda()
  return x,y,z
end


local net = nn.Sequential()
net:add(nn.JoinTable(2, 4))
net:add(nn.Linear(2*inputDim, hSize1))
net:add(nn.PReLU())
net:add(nn.Linear(hSize1, hSize2))
net:add(nn.PReLU())
net:add(nn.Linear(hSize2, hSize3))
net:add(nn.PReLU())
net:add(nn.Linear(hSize3, 1))

criterion = nn.MSECriterion()

net:cuda()
criterion:cuda()

W, gradW = net:getParameters()

lr = 0.005

for i=1,200000 do

  x,y,z = getData(batchSize)

	feval = function()
  	net:zeroGradParameters()
   	t = net:forward{x,y}
  	err = criterion:forward(t, z)
  	df = criterion:backward(t, z)
  	net:backward({x, y}, df)
		return err, gradW
	end

  optim.adam(feval, W, {learningRate=lr})

  if i % 500 == 0 then
    lr = lr * 0.95
  end

  print(i,err)

end

n = 10000

x,y,z = getData(n)
t = net:forward{x,y}
err = criterion:forward(t, z)

print(err)

net:clearState()
torch.save('mlp_rotat.t7', net)
