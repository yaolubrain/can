require 'nn'
require 'cutorch'
require 'cunn'
require 'nngraph'
require 'optim'
require 'hdf5'
require 'data'
require 'SparseMax'

local n = 20
local d = 121
local hSize = 20
local hSize2 = 10
local max_z = 5
local epsilon = 1e-8

input1 = nn.Identity()()
input2 = nn.Identity()()
l_encoder1 = nn.Sequential()
l_encoder2 = nn.Sequential()
l_encoder1:add(nn.Identity())
l_encoder2:add(nn.Identity())
l_f1 = nn.Linear(d,hSize,false)
l_f2 = nn.Linear(d,hSize,false)
l_f1.weight:abs()
l_f2.weight:abs()
l_f1.weight:cdiv(l_f1.weight:sum(2):expandAs(l_f1.weight))
l_f2.weight:cdiv(l_f2.weight:sum(2):expandAs(l_f2.weight))

l_encoder1:add(l_f1)
l_encoder2:add(l_f2)

l_encoder2:share(l_encoder1,'weight','bias','gradWeight','gradBias')

l_encoder = nn.ParallelTable()
l_encoder:add(l_encoder1)
l_encoder:add(l_encoder2)

l_RU1 = nn.SOS(hSize,hSize,hSize2,false,true)
l_RU2 = nn.SOS(hSize,hSize,hSize2,false,true)
l_RU1.weight:abs()
l_RU2.weight:abs()

l_motion1 = nn.Sequential()
l_motion2 = nn.Sequential()
l_motion1:add(l_RU1)
l_motion2:add(l_RU2)
--l_motion1:add(nn.Normalize(2))
--l_motion2:add(nn.Normalize(2))
l_motion1:add(nn.SoftMin())
l_motion2:add(nn.SoftMin())

l_motion = nn.Sequential()
l_motion:add(nn.JoinTable(2))

l_output = nn.Sequential()
l_output:add(nn.Linear(2*hSize2,2*hSize2))
l_output:add(nn.PReLU())
l_output:add(nn.Linear(2*hSize2,2,false))

input_en = l_encoder({input1, input2})
motion1 = l_motion1(input_en)
motion2 = l_motion2(input_en)
motion = l_motion({motion1, motion2})
output = l_output(motion)

net = nn.gModule({input1, input2}, {output})

criterion = nn.MSECriterion()

parameters, gradParameters = net:getParameters()

W_RU1 = l_RU1.weight
W_RU2 = l_RU2.weight
gradW_RU1 = l_RU1.gradWeight
gradW_RU2 = l_RU2.gradWeight

W_f1 = l_f1.weight
W_f2 = l_f2.weight
gradW_f1 = l_f1.gradWeight
gradW_f2 = l_f2.gradWeight

W_1, gradW_1 = l_encoder:getParameters()
W_2, gradW_2 = l_output:getParameters()

lr_mul = 0.005
lr_add = 0.01

for i=1,10000 do

--  x,y,z = data.patch(n)
  x,y,z = data.randomPatch(n,11,3)
  x:resize(n,d)
  y:resize(n,d)


	net:zeroGradParameters()
	t = net:forward{x,y}
	err = criterion:forward(t, z)
	df = criterion:backward(t, z)
	net:backward({x, y}, df)

	feval1 = function()
		return err, gradW_1
	end
	feval2 = function()
		return err, gradW_2
	end

--  optim.adam(feval1, W_1, {learningRate=lr_add})
  optim.adam(feval2, W_2, {learningRate=lr_add})

  gradPos1 = 0.5 * (torch.abs(gradW_RU1) + gradW_RU1) + epsilon
  gradPos2 = 0.5 * (torch.abs(gradW_RU2) + gradW_RU2) + epsilon
  gradNeg1 = 0.5 * (torch.abs(gradW_RU1) - gradW_RU1) + epsilon
  gradNeg2 = 0.5 * (torch.abs(gradW_RU2) - gradW_RU2) + epsilon

  W_RU1:cmul(torch.pow(torch.cdiv(gradNeg1, gradPos1), lr_mul)) 
  W_RU2:cmul(torch.pow(torch.cdiv(gradNeg2, gradPos2), lr_mul)) 

  gradPos_f1 = 0.5 * (torch.abs(gradW_f1) + gradW_f1) + epsilon
  gradNeg_f1 = 0.5 * (torch.abs(gradW_f1) - gradW_f1) + epsilon

  W_f1:cmul(torch.pow(torch.cdiv(gradNeg_f1, gradPos_f1), lr_mul)) 
  W_f1:cdiv(W_f1:sum(2):expandAs(W_f1))

  if i % 500 == 0 then
    lr_add = lr_add * 0.9
    lr_mul = lr_mul * 0.9
  end

  print(i,err)

end

x,y,z = data.randomPatch(n,11,3)
x:resize(n,d)
y:resize(n,d)


t = net:forward{x,y}
err = criterion:forward(t, z)

print(err)

torch.save('sos_2d.t7', net)
