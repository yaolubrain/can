require 'nn'
require 'cutorch'
require 'cunn'
require 'optim'
require 'hdf5'

local n = 100
local d = 121
local hSize = 1200
local hSize2 = 800
local hSize3 = 400
local epsilon = 1e-10

local file = hdf5.open('/share/bayes/yao/cifar10/cifar10_scale_train.h5', 'r')
local X = file:read('X'):all():cuda()
local Y = file:read('Y'):all():cuda()
local Z = file:read('Z'):all():cuda()
file:close()

X = X / 255 - 0.5
Y = Y / 255 - 0.5

function data_scale(n)
  local index = torch.random(1, 500000 - n)
  local x = X[{{index,index+n},{}}]
  local y = Y[{{index,index+n},{}}]
  local z = Z[{{index,index+n},{}}]
  return x,y,z
end


local net = nn.Sequential()
net:add(nn.JoinTable(2, 4))
net:add(nn.Linear(2*d, hSize))
net:add(nn.PReLU())
net:add(nn.Linear(hSize, hSize))
net:add(nn.PReLU())
net:add(nn.Linear(hSize, hSize3))
net:add(nn.PReLU())
net:add(nn.Linear(hSize3, 1))

criterion = nn.MSECriterion()

net:cuda()
criterion:cuda()

W, gradW = net:getParameters()

lr = 0.005

for i=1,200000 do

  x,y,z = data_scale(n)

	feval = function()
  	net:zeroGradParameters()
   	t = net:forward{x,y}
  	err = criterion:forward(t, z)
  	df = criterion:backward(t, z)
  	net:backward({x, y}, df)
		return err, gradW
	end

  optim.adam(feval, W, {learningRate=lr})

  if i % 500 == 0 then
    lr = lr * 0.95
  end

  print(i,err)

end

x,y,z = data_scale(n)
t = net:forward{x,y}
err = criterion:forward(t, z)

print(err)

net:clearState()
torch.save('mlp_scale.t7', net)
