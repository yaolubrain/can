require 'nn'
require 'cutorch'
require 'cunn'
require 'nngraph'
require 'optim'

local n = 20
local d = 11
local hSize = 20
local hSize2 = 20
local max_z = 5
local epsilon = 1e-8

input1 = nn.Identity()()
input2 = nn.Identity()()
l_encoder1 = nn.Sequential()
l_encoder2 = nn.Sequential()
l_encoder1:add(nn.Identity())
l_encoder2:add(nn.Identity())
l_encoder1:add(nn.Linear(d,hSize,false))
l_encoder2:add(nn.Linear(d,hSize,false))
l_encoder1:add(nn.Normalize(2))
l_encoder2:add(nn.Normalize(2))

l_encoder2:share(l_encoder1,'weight','bias','gradWeight','gradBias')

l_encoder = nn.ParallelTable()
l_encoder:add(l_encoder1)
l_encoder:add(l_encoder2)

l_R = nn.OuterProd()
l_motion = nn.Sequential()
l_motion:add(l_R)
l_motion:add(nn.Reshape(hSize2*hSize2))

l_output = nn.Sequential()
--l_output:add(nn.MulConstant(-1))
l_output:add(nn.Normalize(1))
--l_output:add(nn.SoftMax())
--l_output:add(nn.SoftMin())
--l_output:add(nn.Normalize(2))
l_output:add(nn.Linear(hSize2*hSize2,hSize2))
l_output:add(nn.PReLU())
l_output:add(nn.Linear(hSize2,hSize2))
l_output:add(nn.PReLU())
l_output:add(nn.Linear(hSize2,1))

input_en = l_encoder({input1, input2})
motion = l_motion(input_en)
output = l_output(motion)

net = nn.gModule({input1, input2}, {output})

criterion = nn.MSECriterion()

parameters, gradParameters = net:getParameters()

lr = 0.0005

function getData(n)
	local x = torch.randn(n, d)
	local y = torch.zeros(n, d)
	local z = torch.Tensor(n) 
	for j=1,n do
		z[j] = torch.random(-max_z,max_z)
		if z[j] > 0 then    
			y[{j,{1+z[j],d}}] = x[{j,{1,d-z[j]}}]
--			y[{j,{1,z[j]}}] = x[{j,{d-z[j]+1,d}}]
      y[{j,{1,z[j]}}] = torch.randn(1, z[j])
		elseif z[j] < 0 then
			y[{j,{1,d+z[j]}}] = x[{j,{1-z[j],d}}]
--			y[{j,{d+z[j]+1,d}}] = x[{j,{1,-z[j]}}]
      y[{j,{d+z[j]+1,d}}] = torch.randn(1,-z[j])
		else
			y[j] = x[j]
		end
	end

  return x,y,z
end

W, gradW = net:getParameters()

lr = 0.05

for i=1,10000 do

  x,y,z = getData(n)

	net:zeroGradParameters()
	t = net:forward{x,y}
	err = criterion:forward(t, z)
	df = criterion:backward(t, z)
	net:backward({x, y}, df)

	feval = function()
		return err, gradW
	end

  optim.adam(feval, W, {learningRate=lr})

  if i % 500 == 0 then
    lr = lr * 0.8
  end

  print(i,err)

end

x,y,z = getData(10000)
t = net:forward{x,y}
err = criterion:forward(t, z)

print(err)

torch.save('R_1d.t7', net)
