require 'nn'
require 'cutorch'
require 'cunn'
require 'nngraph'
require 'optim'
require 'hdf5'
require 'data'

local file = hdf5.open('/share/bayes/yao/cifar10/cifar10_rotat_45_test.h5', 'r')
local X = file:read('X'):all():cuda()
local Y = file:read('Y'):all():cuda()
local Z = file:read('Z'):all():cuda()
file:close()

X = X / 255 - 0.5
Y = Y / 255 - 0.5

mlp = torch.load('mlp_rotat_45.t7')
--mlp = torch.load('mlp_2d.t7')

sos = torch.load('sosmf_rotat_45.t7')

criterion = nn.MSECriterion()
criterion:cuda()

err = 0
for i=1,10 do
  local x = X[{{(i-1)*10000+1,i*10000},{}}]
  local y = Y[{{(i-1)*10000+1,i*10000},{}}]
  local z = Z[{{(i-1)*10000+1,i*10000},{}}]
  t = mlp:forward{x,y}
  err =  err + criterion:forward(t, z)
end
print(err/10)


err = 0
for i=1,10 do
  local x = X[{{(i-1)*10000+1,i*10000},{}}]
  local y = Y[{{(i-1)*10000+1,i*10000},{}}]
  local z = Z[{{(i-1)*10000+1,i*10000},{}}]
  t = sos:forward{x,y}
  err =  err + criterion:forward(t, z)
end

print(err/10)

