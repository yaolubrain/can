require 'nn'
require 'cutorch'
require 'cunn'
require 'nngraph'
require 'optim'
require 'hdf5'
require 'data'
require 'model'
require 'train'

local inputDim = 121
local outputDim = 6
local lr_add = 0.005
local batchSize = 100
local epochNum = 200000

net = model.MLP({inputDim, 1200, 300, 100, 100, outputDim})
net:cuda()

criterion = nn.MSECriterion()
criterion:cuda()

X, Y, Z = data.train("projective")
train.NN(batchSize, epochNum, lr_add)

net:clearState()
torch.save('models/mlp_projective.t7', net)
