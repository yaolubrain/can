require 'nn'
require 'cutorch'
require 'cunn'
require 'nngraph'
require 'optim'
require 'hdf5'
require 'data'
require 'SparseMax'

local n = 100
local d = 121
local hSize = d
local hSize2 = 200
local hSize3 = 200
local max_z = 5
local epsilon = 1e-20

input1 = nn.Identity()()
input2 = nn.Identity()()

l_RU = nn.SOS(hSize,hSize,hSize2,false,true)
l_RU.weight:abs()

l_integ = nn.Linear(hSize2, hSize3):noBias()
l_integ.weight:zero()
for i=1,hSize3 do
  local gSize = math.floor(hSize2/hSize3)
  l_integ.weight[{i,{(i-1)*gSize+1, i*gSize}}]:fill(1)
end

l_motion = nn.Sequential()
l_motion:add(l_RU)
l_motion:add(l_integ)
--l_output:add(nn.SparseMax())
l_motion:add(nn.SoftMin())

l_output = nn.Sequential()
--l_output:add(nn.MulConstant(-1))
--l_output:add(nn.SparseMax())
--l_output:add(nn.Normalize(1))
l_output:add(nn.Linear(hSize3,hSize3))
l_output:add(nn.PReLU())
l_output:add(nn.Linear(hSize3,2,false))

--input1_en = l_encoder1(input1)
--input2_en = l_encoder2(input2)
--input_en = l_encoder({input1, input2})
--motion = l_motion({input1_en, input2_en})
--motion = l_motion(input_en)
motion = l_motion({input1, input2})
output = l_output(motion)

net = nn.gModule({input1, input2}, {output})

criterion = nn.MSECriterion()

net:cuda()
criterion:cuda()

parameters, gradParameters = net:getParameters()

W = l_RU.weight
gradW = l_RU.gradWeight

W_integ = l_integ.weight
gradW_integ = l_integ.gradWeight

W_2, gradW_2 = l_output:getParameters()

lr_mul = 0.005
lr_add = 0.001

for i=1,200000 do

--  x,y,z = data.randomPatch1D(n,11,max_z)
--  x:resize(n,d)
--  y:resize(n,d)

--  x,y,z = data.randomPatch(n,11,3)
--
  x,y,z = data.patch(n,11,max_z)
--  x:resize(n,d)
--  y:resize(n,d)

  x = x:cuda()
  y = y:cuda()
  z = z:cuda()

--[[
  l_RU.gradW1Pos:zero()
  l_RU.gradW2Pos:zero()
  l_RU.gradW1Neg:zero()
  l_RU.gradW2Neg:zero()
--]]
	net:zeroGradParameters()
	t = net:forward{x,y}
	err = criterion:forward(t, z)
	df = criterion:backward(t, z)
	net:backward({x, y}, df)

	feval1 = function()
    return err, gradParameters
	end

	feval2 = function()
		return err, gradW_2
	end
	feval3 = function()
		return err, gradW
	end

--  optim.adam(feval1, W_1, {learningRate=lr_add})
--  optim.adam(feval1, parameters, {learningRate=lr_add})
  --optim.adam(feval2, W_2, {learningRate=lr_add})
  optim.adam(feval2, W_2, {learningRate=lr_add})
--  optim.adam(feval3, W, {learningRate=0.0001})
--  W:abs()
--  optim.adam(feval3, W_integ, {learningRate=lr_add})
--  optim.adadelta(feval1, W_1, {learningRate=lr_add})
--  optim.adadelta(feval2, W_2, {learningRate=lr_add})
  gradPos = 0.5 * (torch.abs(gradW) + gradW) + epsilon 
  gradNeg = 0.5 * (torch.abs(gradW) - gradW) + epsilon
--  W:cmul(torch.pow(0.1 * torch.cdiv(gradNeg, gradPos), lr_mul)) 
  W:cmul(lr_mul * 0.1 * torch.cdiv(gradNeg, gradPos)) 

--
--[[
  if i % 10 >= 5 then
    W[1]:cmul(torch.pow(torch.cdiv(gradNeg[1], gradPos[1]), lr_mul))  
  else 

    W[2]:cmul(torch.pow(torch.cdiv(gradNeg[2], gradPos[2]), lr_mul))  
  end
--]]
--[[
  gradPos_integ = 0.5 * (torch.abs(gradW_integ) + gradW_integ) + epsilon
  gradNeg_integ = 0.5 * (torch.abs(gradW_integ) - gradW_integ) + epsilon
  W_integ:cmul(torch.pow(torch.cdiv(gradNeg_integ, gradPos_integ), lr_mul)) 
--]]
--[[
  for i=1,W:size(1) do
    W[i]:div(W[i]:sum())
  end
  gradPos_f1 = 0.5 * (torch.abs(gradW_f1) + gradW_f1) + epsilon
  gradNeg_f1 = 0.5 * (torch.abs(gradW_f1) - gradW_f1) + epsilon

--]]
--  gradPos_f2 = 0.5 * (torch.abs(gradW_f2) + gradW_f2) + epsilon
--  gradNeg_f2 = 0.5 * (torch.abs(gradW_f2) - gradW_f2) + epsilon

--  W_f1:cmul(torch.pow(torch.cdiv(gradNeg_f1, gradPos_f1), lr_mul)) 
--  W_f1:cdiv(W_f1:sum(2):expandAs(W_f1))

--  W_f1:cmul(W_f1:clone():pow(2):sum(2):pow(-0.5):expandAs(W_f1))
--  W_f2:cmul(torch.pow(torch.cdiv(gradNeg_f2, gradPos_f2), lr_mul)) 
  if i % 500 == 0 then
    lr_add = lr_add * 0.9
    lr_mul = lr_mul * 0.9
  end

  print(i,err)

end

n = 10000
--x,y,z = data.randomPatch1D(n,11,max_z)
--x:resize(n,d)
--y:resize(n,d)


x,y,z = data.patch(n,11,max_z)
x = x:cuda()
y = y:cuda()
z = z:cuda()


t = net:forward{x,y}
err = criterion:forward(t, z)

print(err)

torch.save('sos_2d.t7', net)
