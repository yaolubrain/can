require 'nn'
require 'cutorch'
require 'cunn'
require 'optim'
require 'hdf5'

local n = 100
local d = 1024
local hSize = 200
local hSize2 = 1200
local hSize3 = 100
local epsilon = 1e-10


local file = hdf5.open('/share/bayes/yao/coil-100/coil100.h5', 'r')
local img = file:read('img'):all():float()

img = img / 255 - 0.5

function data_rotat3D(n)

  X = torch.Tensor(n, d)
  Y = torch.Tensor(n)

  for i=1,n do

    local obj = torch.random(1, 10)
    local orient = torch.random(1, 72)

    X[i] = img[{obj,orient,{}}]
    Y[i] = obj
  end

  X = X:cuda()
  Y = Y:cuda()

  return X,Y
end


local net = nn.Sequential()
net:add(nn.Linear(d, hSize))
net:add(nn.PReLU())
net:add(nn.Linear(hSize, hSize))
net:add(nn.PReLU())
net:add(nn.Linear(hSize, 10))
net:add(nn.SoftMax())

--criterion = nn.MSECriterion()
--criterion = nn.ClassNLLCriterion()
criterion = nn.CrossEntropyCriterion()
net:cuda()
criterion:cuda()

confusion = optim.ConfusionMatrix(10)

W, gradW = net:getParameters()

lr = 0.001

for i=1,1000 do

  x,y = data_rotat3D(n)

	feval = function()
  	net:zeroGradParameters()
   	t = net:forward(x)

  	err = criterion:forward(t, y)
  	df = criterion:backward(t, y)
  	net:backward(x, df)
		return err, gradW
	end

  optim.adam(feval, W, {learningRate=lr})

  for j=1,n do
    confusion:add(t[j], y[j])
  end


  if i % 500 == 0 then
    lr = lr * 0.9
  end

  print(i,err)

end

x,y = data_rotat3D(1000)
t = net:forward(x)
confusion:zero()
for j=1,n do
  confusion:add(t[j], y[j])
end
print(confusion)

net:clearState()
torch.save('mlp_rotat_45.t7', net)
