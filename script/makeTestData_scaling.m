clear all

patchSize = 11;
imageSize = 32;
patchNumPerImage = 10;
imageNum = 10000;

f = 'test_batch.mat';
batch = importdata(f);
imageRGB = reshape(batch.data, [10000 imageSize imageSize 3]);
imageRGB = permute(imageRGB, [2 3 4 1]);

X = zeros(patchSize^2, imageNum*patchNumPerImage);
Y = zeros(patchSize^2, imageNum*patchNumPerImage);
Z = zeros(2, imageNum*patchNumPerImage);

index = 1;
for t = 1:imageNum

  disp(t)

  I = rgb2gray(imageRGB(:,:,:,t));

  for i = 1:patchNumPerImage

    z = rand(2,1) * 1.5 + 0.5;

    T = eye(3);
    T(1,1) = z(1);
    T(2,2) = z(2);

    tform = maketform('affine', T);
    J = imtransform(I, tform);

    row_x_1 = floor(size(I,1) / 2) - floor(patchSize/2) + 1;
    row_x_2 = floor(size(I,1) / 2) + floor(patchSize/2) + 1;
    col_x_1 = floor(size(I,2) / 2) - floor(patchSize/2) + 1;
    col_x_2 = floor(size(I,2) / 2) + floor(patchSize/2) + 1;

    row_y_1 = floor(size(J,1) / 2) - floor(patchSize/2) + 1;
    row_y_2 = floor(size(J,1) / 2) + floor(patchSize/2) + 1;
    col_y_1 = floor(size(J,2) / 2) - floor(patchSize/2) + 1;
    col_y_2 = floor(size(J,2) / 2) + floor(patchSize/2) + 1;

    x = I(row_x_1:row_x_2, col_x_1:col_x_2);
    y = J(row_y_1:row_y_2, col_y_1:col_y_2);

    X(:,index) = x(:)';
    Y(:,index) = y(:)';
    Z(:,index) = z(:)';

    index = index + 1;
  end
end

h5create('scaling_test.h5','/X',size(X))
h5create('scaling_test.h5','/Y',size(Y))
h5create('scaling_test.h5','/Z',size(Z))
h5write('scaling_test.h5','/X',X)
h5write('scaling_test.h5','/Y',Y)
h5write('scaling_test.h5','/Z',Z)
