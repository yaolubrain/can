require 'nn'
require 'cutorch'
require 'cunn'
require 'nngraph'
require 'optim'
require 'hdf5'
require 'data'
require 'test'

err_file = hdf5.open('err_translation.h5', 'w')

ctn = torch.load('models/ctn_translation.t7'):cuda()
bln = torch.load('models/bln_translation.t7'):cuda()
can = torch.load('models/can_translation.t7'):cuda()

criterion = nn.MSECriterion()
criterion:cuda()

X, Y, Z = data.test('translation')

err_ctn = test.computeParameterError(ctn):float()
err_bln = test.computeParameterError(bln):float()
err_can = test.computeParameterError(can):float()
print(err_ctn:mean())
print(err_bln:mean())
print(err_can:mean())
err_file:write('parameter_ctn', err_ctn)
err_file:write('parameter_bln', err_bln)
err_file:write('parameter_can', err_can)

err_ctn = test.computeTransformationError(ctn, 'translation'):float()
err_bln = test.computeTransformationError(bln, 'translation'):float()
err_can = test.computeTransformationError(can, 'translation'):float()
print(err_ctn:mean())
print(err_bln:mean())
print(err_can:mean())
err_file:write('transformation_ctn', err_ctn)
err_file:write('transformation_bln', err_bln)
err_file:write('transformation_can', err_can)

err_file:close()


