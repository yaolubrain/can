require 'nn'
require 'cutorch'
require 'cunn'
require 'nngraph'
require 'optim'
require 'hdf5'
require 'data'
require 'train'
require 'model'

local batchSize = 100
local inputDim = 121
local hSize1 = inputDim
local hSize2 = 800
local hSize3 = 200
local outputSize = 2
local epsilon = 1e-20
local lr_mul = 0.005
local lr_add = 0.005
--local lr_add = 0.001 
local epochNum = 200000

net = model.SOSNet(hSize1, hSize2, hSize3, outputSize)
net:cuda()

criterion = nn.MSECriterion()
criterion:cuda()

X, Y, Z = data.trans()
train.SOS(batchSize, epochNum, lr_add, lr_mul, epsilon)

net:clearState()
torch.save('sosmf_trans.t7', net)
