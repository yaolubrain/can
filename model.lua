model = {}

function model.CAN(netSize)

  local CAN = nn.CAU(netSize[1],netSize[1],netSize[2]):noBias()

  local Pool = nn.Linear(netSize[2],netSize[3]):noBias()
  Pool.weight:zero()
  for i=1,netSize[3] do
    local gSize = math.floor(netSize[2]/netSize[3])
    Pool.weight[{i,{(i-1)*gSize+1, i*gSize}}]:fill(1)
  end

  local l_relation = nn.Sequential()
  l_relation:add(CAN)
  l_relation:add(Pool)
  l_relation:add(nn.SoftMin())

  local l_decision = nn.Sequential()
  l_decision:add(nn.Linear(netSize[3],netSize[4]):noBias())
  l_decision:add(nn.PReLU())
  l_decision:add(nn.Linear(netSize[4],netSize[5]):noBias())
  l_decision:add(nn.PReLU())
  l_decision:add(nn.Linear(netSize[5],netSize[6]):noBias())

  local input1 = nn.Identity()()
  local input2 = nn.Identity()()
  local relation = l_relation({input1, input2})
  local output = l_decision(relation)
  local net = nn.gModule({input1, input2}, {output})

  return net
end

function model.BLN(netSize)

  local Bilinear = nn.BilinearMF(netSize[1],netSize[1],netSize[2]):noBias()
  local Pool = nn.Linear(netSize[2],netSize[3]):noBias()
  Pool.weight:zero()
  for i=1,netSize[3] do
    local gSize = math.floor(netSize[2]/netSize[3])
    Pool.weight[{i,{(i-1)*gSize+1, i*gSize}}]:fill(1)
  end

  local l_relation = nn.Sequential()
  l_relation:add(Bilinear)
  l_relation:add(Pool)
  l_relation:add(nn.Normalize(2))

  local l_decision = nn.Sequential()
  l_decision:add(nn.Linear(netSize[3],netSize[4]):noBias())
  l_decision:add(nn.PReLU())
  l_decision:add(nn.Linear(netSize[4],netSize[5]):noBias())
  l_decision:add(nn.PReLU())
  l_decision:add(nn.Linear(netSize[5],netSize[6]):noBias())

  local input1 = nn.Identity()()
  local input2 = nn.Identity()()
  local relation = l_relation({input1, input2})
  local output = l_decision(relation)
  local net = nn.gModule({input1, input2}, {output})

  return net
end

function model.MLP(netSize)
  local net = nn.Sequential()
  net:add(nn.JoinTable(2, 4))
  net:add(nn.Linear(2*netSize[1], netSize[2]))
  net:add(nn.PReLU())
  net:add(nn.Linear(netSize[2], netSize[3]))
  net:add(nn.PReLU())
  net:add(nn.Linear(netSize[3], netSize[4]))
  net:add(nn.PReLU())
  net:add(nn.Linear(netSize[4], netSize[5]))
  net:add(nn.PReLU())
  net:add(nn.Linear(netSize[5], netSize[6]))

  return net
end

return model

