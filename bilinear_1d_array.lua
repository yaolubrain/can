require 'nn'
require 'cutorch'
require 'cunn'
require 'nngraph'
require 'optim'
require 'data'

local n = 20
local d = 11
local hSize = 11
local hSize2 = 5
local max_z = 5
local epsilon = 1e-8

input1 = nn.Identity()()
input2 = nn.Identity()()
l_encoder1 = nn.Sequential()
l_encoder2 = nn.Sequential()
l_encoder1:add(nn.Identity())
l_encoder2:add(nn.Identity())
l_encoder1:add(nn.Linear(d,hSize,false))
l_encoder2:add(nn.Linear(d,hSize,false))
l_encoder1:add(nn.Normalize(2))
l_encoder2:add(nn.Normalize(2))
l_encoder2:share(l_encoder1,'weight','bias','gradWeight','gradBias')

l_encoder = nn.ParallelTable()
l_encoder:add(l_encoder1)
l_encoder:add(l_encoder2)

l_bilinear = nn.Bilinear(hSize,hSize,hSize2,false,true)
l_motion = nn.Sequential()
l_motion:add(l_bilinear)

l_output = nn.Sequential()
--l_output:add(nn.MulConstant(-1))
l_output:add(nn.Normalize(2))
--l_output:add(nn.SoftMax())
--l_output:add(nn.SoftMin())
--l_output:add(nn.Normalize(2))
l_output:add(nn.Linear(hSize2,hSize2))
l_output:add(nn.PReLU())
l_output:add(nn.Linear(hSize2,hSize2))
l_output:add(nn.PReLU())
l_output:add(nn.Linear(hSize2,1))

--input1_en = l_encoder1(input1)
--input2_en = l_encoder2(input2)
input_en = l_encoder({input1, input2})
--motion = l_motion({input1_en, input2_en})
motion = l_motion(input_en)
output = l_output(motion)

net = nn.gModule({input1, input2}, {output})

criterion = nn.MSECriterion()

parameters, gradParameters = net:getParameters()

lr = 0.05

W, gradW = net:getParameters()


for i=1,10000 do

  x,y,z = data.random1D(n, d, max_z)

	net:zeroGradParameters()
	t = net:forward{x,y}
	err = criterion:forward(t, z)
	df = criterion:backward(t, z)
	net:backward({x, y}, df)

	feval = function()
		return err, gradW
	end

  optim.adam(feval, W, {learningRate=lr})

  if i % 500 == 0 then
    lr = lr * 0.8
  end

  print(i,err)

end

x,y,z = data.random1D(n, d, max_z)
t = net:forward{x,y}
err = criterion:forward(t, z)

print(err)

torch.save('bilinear_1d.t7', net)
