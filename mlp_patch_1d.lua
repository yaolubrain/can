require 'nn'
require 'cutorch'
require 'cunn'
require 'nngraph'
require 'optim'
require 'hdf5'
require 'data'
require 'SparseMax'

local n = 20
local d = 121
local hSize = 50
local max_z = 5
local epsilon = 1e-8

local net = nn.Sequential()

net:add(nn.JoinTable(2))

net:add(nn.Linear(2*121, hSize))
net:add(nn.PReLU())
net:add(nn.Linear(hSize, hSize))
net:add(nn.PReLU())
net:add(nn.Linear(hSize, hSize))
net:add(nn.PReLU())
net:add(nn.Linear(hSize, 1))

criterion = nn.MSECriterion()

W, gradW = net:getParameters()

lr = 0.005

for i=1,10000 do

  x,y,z = data.randomPatch1D(n)

	net:zeroGradParameters()
	t = net:forward{x,y}
	err = criterion:forward(t, z)
	df = criterion:backward(t, z)
	net:backward({x, y}, df)

	feval = function()
		return err, gradW
	end

  optim.adam(feval, W, {learningRate=lr})

  if i % 500 == 0 then
    lr = lr * 0.8
  end

  print(i,err)

end

x,y,z = data.randomPatch1D(10000)
t = net:forward{x,y}
err = criterion:forward(t, z)

print(err)

torch.save('mlp_patch_1d.t7', net)
